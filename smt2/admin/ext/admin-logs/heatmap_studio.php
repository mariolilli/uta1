<?php
// Start the session
session_start();
?>

<?php
$id_task = $_POST['taskid']; //Recupero Identificativo del task su cui lavorare
$url = $_POST['url_'];        //Recupero url del task 
//echo $id_task;
//echo $url;
?>

<?php require '../../../config.php'; ?>



<!DOCTYPE HTML>
<html>
	<head>
    <script src="../../js/simpleheat.js"></script>
    <script src="../../js/data.js"></script>
    <title>Show Heatmap</title>
         <style>
        .options { position: relative; top: 0; right: 0; padding: 10px; background: rgba(255,255,255,0.6);
            border-bottom: 1px solid #ccc; border-left: 1px solid #ccc; line-height: 1; }
    </style>
	</head>
	<body>
        
		<br />
		
				
	
					<?php
			
						$servername = DB_HOST;
						$username = DB_USER;
						$password = DB_PASSWORD;
						$dbname = DB_NAME;
				
						// Create connection
						$conn = new mysqli($servername, $username, $password, $dbname);
						// Check connection
						if ($conn->connect_error) {
				    		die("Connection failed: " . $conn->connect_error);
						} 
				
                        //query che seleziona le coordinate x e y relative al task
						$sql = "SELECT smt2_records.coords_x, smt2_records.coords_y, smt2_records.vp_width, smt2_records.vp_height
                                FROM smt2_records 
                                JOIN smt2_ass_task_users_records 
                                ON smt2_records.id = smt2_ass_task_users_records.id_records 
                                JOIN smt2_cache
                                ON smt2_cache.id = smt2_records.cache_id 
                                WHERE smt2_ass_task_users_records.id_task =".$id_task."
                                AND smt2_cache.url='".$url."'";


						$result = $conn->query($sql);
				        $vettoreX = array(); //vettore che conterrà la concatenazione fra i vari task delle coordinate X
                        $vettoreY = array(); //vettore che conterrà la concatenazione fra i vari task delle coordinate Y
						
                            if ($result->num_rows > 0) {
                            //prelevo dimensione url sito del task
                            $riga = $result->fetch_assoc();
                            $width = $riga["vp_width"];
                            $height = $riga["vp_height"];
                            
                            $cordX = explode(",",$riga["coords_x"]); //split sulla virgola della riga delle coordinate x
                            $vettoreX = array_merge($vettoreX,$cordX);//concatenazione
                            $cordY = explode(",",$riga["coords_y"]); //split sulla virgola della riga delle coordinate y
                            $vettoreY = array_merge($vettoreY,$cordY);//concatenazione
                    
                    
				    		// output data of each row
                            while ($row = $result->fetch_assoc()){
                    
                                
                            
                                $cordX = explode(",",$row["coords_x"]); //split sulla virgola della riga delle coordinate x
                                $vettoreX = array_merge($vettoreX,$cordX);//concatenazione
                                $cordY = explode(",",$row["coords_y"]); //split sulla virgola della riga delle coordinate y
                                $vettoreY = array_merge($vettoreY,$cordY);//concatenazione
                            
                           
                               
                                                                } //fine while
                                                    }
        
                    else { //se non trova coordinate corrispondeti al task stampa un alert
                        $message = "Non è stato rilevato heatmap per questo task";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                         }
                        //else echo("<h3>Non è stato rilevato l'heatmap per questo task</h3></br>");
                       //print_r($vettoreX);   //per verifica di concatenazione avvenuta correttamente
                       //echo count($vettoreX); //per verifica di concatenazione avvenuta correttamente         
				?>
			
        <!-- div per contenere il settings dell'heatmap -->
        <div class="options">
        <label>Radius </label><input type="range" id="radius" value="25" min="10" max="50" /><br />
        <label>Blur </label><input type="range" id="blur" value="15" min="10" max="50" />
        </div>  
        <!-- inserisco width e heigh sia del canvas che dell'iframe in base alle dimensioni della pagina rilevata per il task -->
        <canvas id="canvas" width="<?php echo $width ?>" height="<?php echo $height ?>" style="position: absolute;top:100px"></canvas>
        <iframe id ="iframe_id" class="embed-responsive-item" src="<?php echo $url ?>"  width="<?php echo $width ?>" height="<?php echo $height ?>" style="overflow:hidden;" allowfullscreen>  
        </iframe>        
        
		<!-- SCRIPT PER VISUALIZZARE MAPPA HEATMAP -->
        <script>
            /*window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                              window.webkitRequestAnimationFrame || window.msRequestAnimationFrame; */
            if (!window.requestAnimationFrame) {
                                                window.requestAnimationFrame = (window.webkitRequestAnimationFrame ||
                                                window.mozRequestAnimationFrame ||
                                                window.msRequestAnimationFrame ||
                                                window.oRequestAnimationFrame ||
                                                function (callback) {
                                                    return window.setTimeout(callback, 17 /*~ 1000/60*/);
                                                                    });
                                                }
            
            function get(id) {
                                return document.getElementById(id);
                             }

            var heat = simpleheat('canvas').data(data).max(100),frame; //chiamo costruttore simpleheatmpat
            <?php 
            
            //iterazione per numero di elementi del vettore che contiene le coordinate del mouse
            for ($j=0; $j<count($vettoreX); $j++){ //sarebbe uguale anche count($vettoreY);
                                                    ?> 
                                                    //aggiungo coordinate al data di simpleheatmap
                                                    heat.add([<?php echo $vettoreX[$j]?>,<?php echo $vettoreY[$j]?>,10]); 
                                                    <?php
                                                  }        
                                                    ?>

            //FUNZIONE PER DISEGNARE HEATMAP
            function draw() {
                            console.time('a');
                            heat.draw();
                            console.timeEnd('a');
                            frame = null;
                            }
        draw();   
        
            var radius = get('radius'),
            blur = get('blur'),
            changeType = 'oninput' in radius ? 'oninput' : 'onchange';

            radius[changeType] = blur[changeType] = function (e) {
            heat.radius(+radius.value, +blur.value);
            frame = frame || window.requestAnimationFrame(draw);
                                                                };
        </script>
 
	</body>
</html>
