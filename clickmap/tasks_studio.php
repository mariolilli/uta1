<?php
// Start the session
// session_start();
require_once ("../lib/config.php");
include_once ("lib/clickmap_db.php");

/*
 * Uncomment the "else" clause below if e.g. userpie is not at the root of your site.
 */
if (! isUserLoggedInEsp()) {
    header("Location: " . ACCOUNT_DIR . " login.php");
}

unset($_SESSION['ob']); // distruggo la variabile di sessione con l'obiettivo del task per visualizzazione Heatmap
unset($_SESSION['ist']); // distruggo la variabile di sessione con la descrizione del task per visualizzazione Heatmap
unset($_SESSION['task_id']); // distruggo la variabile di sessione con l'id task creata per visualizzazione Heatmap
unset($_SESSION['pagine']); // distruggo la variabile di sessione flag per visualizzare l'icona "pagine" nella navbar
?>


<?php
// se non esiste una variabile di sessione sull'id studio la prelevo dal form con POST e imposto una variabile di sessione
if (! isset($_SESSION['idstudio'])) {
    $_SESSION['idstudio'] = $_POST['idstudio']; // Identificativo dello studio da valutare
    $id_studio = $_SESSION['idstudio'];
    // echo "idstudio: ".$id_studio; //stampa di prova
}  // altrimenti imposto la variabile $id_studio con il valore della variabile di sessione già presa
else {
    $id_studio = $_SESSION['idstudio'];
    // echo "idstudio: ".$id_studio; //stampa di prova
}
?>

<?php require '.././smt2/config.php'; ?>
<?php include INC_DIR.'header.php'; ?>

<!DOCTYPE HTML>
<html>
<head>
<title>Tasks Clickmap UTAssistant</title>
<!-- stile per il pulsante indietro -->

</head>
<body>

	<!--mostro a video il titolo dello studio corrente-->

	<!-- Riferimento per tornare indietro alla pagina principale dell'esperto -->

    <?php require_once("../app/inc/navbars/navbar_esperto.php"); ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3">
				<a href="<?php echo ESPERTO_DIR; ?>esperto_home.php"></a>
			</div>
			<div class="col-sm-9">
				<h1 align="center">Clickmap</h1>

             <?php
            $ob_studio = ob_studio($id_studio); // chiamo funzione che effettua query per recuperare nome caso di studio
            $r = $db->sql_fetchrow($ob_studio);

            ?>
		<h4 align="center"><?php echo $r['obiettivo'] ?></h4>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
        
				<!-- MENU RAPIDO HEATMAP -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<b>Studi effettuati</b>
					</div>
					<div class="panel-body">
<?php

$result = $loggedInUser->recupera_studi_esperto(); // recupero tutti i casi di studio dell'esperto
while ($campi = $db->sql_fetchrow($result)) {
    $campo[] = $campi;
}

foreach ($campo as $key => $row) // foreach sui casi di studio
{
    $studi = $row['obiettivo']; // prendo tutti gli obiettivi dei casi di studi uno alla volta

    if (strcmp($studi, $r['obiettivo']) == 0) { // se lo studio è quello corrente nella pagina lo evidenzio nel menù

        echo "<a href=\"javascript:menufunc('menu" . $key . "')\"><span style='background-color:#90EE90'><b><u>$studi</u></b></span></a><br><br>"; // stampo obiettivi nel menu
        echo "<div id='menu" . $key . "'  style=\"display: block;\">"; // esplodo il menù se corrisponde al caso di studio
    }

    else { // altrimenti non evidenzio il caso di studio nel menù

        echo "<a href=\"javascript:menufunc('menu" . $key . "')\"><b><u>$studi</u></b></a><br><br>"; // li stampo sul menu
        echo "<div id='menu" . $key . "'  style=\"display: none;\">";
    }

    // INSERIMENTO OBIETTIVI DEI TASKS DI UN CASO DI STUDIO
    $obiettivi_task = array(); // vettore che conterrà gli obiettivi in ordine dei vari tasks dei casi di studio
    $id_tasks = array(); // vettore che conterrà gli id tasks in ordine
    $result2 = ob_task($row['id_studio']); // chiamo funzione per recuperare gli obiettivi dei tasks di un caso di studio

    while ($campo2 = mysql_fetch_array($result2)) {
        $nome_obiettivo = explode("\n", $campo2['obiettivo']); // split sulle andate a capo per suddividere i vari obiettivi
        $obiettivi_task = array_merge($obiettivi_task, $nome_obiettivo); // inserisco i singoli obiettivi in un array

        $nome_task = explode("\n", $campo2['id_task']); // split sulle andate a capo per suddividere gli id task
        $id_tasks = array_merge($id_tasks, $nome_task); // inserisco i singoli id task in un array
    }

    $i = 0; // contatore per id task
    $key2 += count($campo); // serve per aprire e chiudere il menu ad albero nelle varie sottosezioni
    foreach ($obiettivi_task as $row2) // foreach sui tasks
{
        $id = $id_tasks[$i]; // prelevo l'id task corrispondente all'obiettivo del task che servirà come parametro della funzione rec_pagine
                             // echo $id; //test
        echo "- "; // test
                   // echo $key2; //test

        echo "<a href=\"javascript:menufunc('menu" . $key2 . "')\">$row2</a><br><br>";
        echo "<div id='menu" . $key2 . "'  style=\"display: none;\">";

        $key2 ++; // incremento la chiave per aprire e chiudere i sottomenu

        // INSERIMENTO PAGINE VISUALIZZATE PER TASK
        $pagine_task = array(); // vettore che conterrà le pagine visualizzate per task
        $result3 = rec_pagine($id); // chiamo funzione per recuperare le pagine visualizzate durante un task tramite il suo id in input

        while ($campo3 = mysql_fetch_array($result3)) {
            $nome_aux4 = explode("\n", $campo3['url']); // split sull'andata a capo per le pagine aperte durante il task
            $pagine_task = array_merge($pagine_task, $nome_aux4); // inserisco le pagine aperte durante il task in un vettore
        }

        foreach ($pagine_task as $row3) // foreach sulle pagine aperte durante il task
{
            /*
             * Il link della pagina nell'href porta alla visualizzazione dell'heatmap passando alla pagina tre variabili (idtask,url,obiettivo) che servono per la creazione dell'heatmap; queste variabili sono recuperate con $_REQUEST nella pagina heatmap_studio.php qualora non ci siano le variabili di sessione di idtask e url.
             */
            echo "&nbsp&nbsp&nbsp- "; // spazio
            echo "<a href='heatmap_studio.php?task=" . $id . "&url=" . $row3 . "&obiettivo=" . $row2 . "' target='_blank'><i>$row3</i></a><br><br>";
            echo "<div id='menu" . $key3 . "'  style=\"display: none;\">";

            ?>
        <?php
            echo "</div>"; // chiusura div per pagine
        }
        echo "</div>"; // chiusura div per tasks
        $i ++; // incremento id task per il task successivo
    }
    echo "</div>"; // chiusura div per casi di studio
}

?>

            <!-- SCRIPT PER IL FUNZIONAMENTO DEL MENU AD ALBERO -->
						<script>
            function startmenu(menuId){
               document.getElementById(menuId).style.display = "none";
            }


            function menufunc(menuId)
            {
                if(document.getElementById(menuId).style.display == "none")
                {
                    startmenu(menuId);
                    document.getElementById(menuId).style.display = "block";

                }
                else if(document.getElementById(menuId).style.display == "block")
                {
                    startmenu(menuId);
                    document.getElementById(menuId).style.display = "none";

                } else
                {
                    startmenu(menuId);
                }
            }
            </script>

					</div>
				</div>
			</div>
			<!-- col-lg-3 -->

			<div class="col-sm-9">
				<div class="panel panel-default">

					<div class="panel-heading">
						<b>Tasks</b>
					</div>
					<div class="panel-body">
						<form action="tasks_studio_2.php" method="post">

					<?php

    echo '<table class="table table-hover"><tr><th>Obiettivo</th><th>URL Principale</th><th>Azioni</th></tr><tbody>';

    // Recupero le informazioni sui tasks del caso di studio in questione
    $Dati = info_task($id_studio);
    while ($row = mysql_fetch_array($Dati)) {

        // visualizzo in tabella obiettivi dei tasks e url principale inviando al click idtask e url
        echo '<tr><td>' . $row["obiettivo"] . '</td>
                                <td>' . $row["url"] . '<input id="url_" type="hidden" name="url_" value="' . $row["url"] . '"></input></td>
								<td><button type="submit" class="btn btn-default btn-sm" name="task_id" value="' . $row["id_task"] . '" checked="checked"><span class="glyphicon glyphicon-fire"></span>&nbsp;Clickmap</button>
						  </td></tr>';
    }
    ?>



		</form>
					</div>
				</div>
			</div>
			<!-- col-lg-9 -->
		</div>
		<!-- row -->
	</div>
	<!-- container -->

</body>
</html>
