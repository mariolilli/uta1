<?php

require_once '../../lib/config.php';
?>


<!DOCTYPE html>
<html>
   <head>
      <title>UTAssistant | <?php echo $websiteName; ?> </title>
      <?php require_once("../inc/head_inc.php"); ?>
   </head>

<body>
<?php require_once("../inc/navbars/navbar_default.php"); ?>


<div>

<br />
<br />
<br />
    <strong class="h1" ><center> UTAssistant </center></strong>
<br />
<br />
<br />
<p class="text-center">
  <a class="btn btn-default" href="login.php">Accedi</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-danger btn-large" href="<?php echo ESPERTO_DIR;?>register_esperto.php">Registrati come Esperto</a>
</p>

<br>

<p class="text-center">
<a href="forgot-password.php">Password dimenticata?</a> | <a href="resend-activation.php">Reinvia mail di attivazione</a></p>
<div class="clear"></div>
</div>
</body>
</html>


