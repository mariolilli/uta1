<?php

/*
  Query utilizzate nel sistema
 */

$query = array(
    'TASK_STUDIO' => 'SELECT `id_task`,`obiettivo` FROM `task` WHERE `id_studio` = datoStudio ;',
    'ASS_TASK_UTENTI' => 'SELECT t.id_task, t.obiettivo FROM task as t INNER JOIN ass_task_users_records as atur ON t.id_task = atur.id_user AND atur.id_user = datoUtente ;',
    'UTENTI_STUDIO' => 'SELECT u.user_id,u.username,u.email FROM users as u INNER JOIN ass_studio_users as asu ON u.user_id = asu.id_utente WHERE asu.id_studio = datoStudio AND asu.flag_completato = 1;',
    'ASS_UTENTI_TASK' => 'SELECT u.user_id,u.username,u.email FROM users as u INNER JOIN ass_task_users_records as atur ON u.user_id = atur.id_user AND atur.id_task = datoTask',
    'INSERT_PATH_AUDIO' => "INSERT INTO file_audio(id,task_id,user_id,path)VALUES('datoId','datoTask','datoIdUser','datoUrl')",
    'INSERT_PATH_VIDEO' => "INSERT INTO file_video(id,task_id,user_id,path)VALUES('datoId','datoTask','datoIdUser','datoUrl')",
    'CECH_AUDIO' => 'SELECT "flag_audio" FROM "studio" WHERE "id_studio" = dato;',
    'CECH_VIDEO' => 'SELECT "flag_video" FROM "studio" WHERE "id_studio" = dato;',
    'LOAD_V' => "SELECT * FROM `file_video` WHERE `task_id` = datoT and `user_id` = datoU;",
    'LOAD_A' => "SELECT * FROM `file_audio` WHERE `task_id` = datoT and `user_id` = datoU;",
    'LOAD_VIDEO' => "SELECT * FROM file_video WHERE task_id = datoT and user_id = datoU;",
    "LOAD_AUDIO" => "SELECT * FROM file_audio WHERE task_id = datoT and user_id = datoU;"
);
?>