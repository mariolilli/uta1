<?php
// Start the session
session_cache_limiter('public'); //evitare documento scaduto col pulsante indietro
session_start();
?>

<?php
//Identificativo dello studio da valutare
$id_studio = $_POST['idstudio'];
$_SESSION["idstudio"] = $id_studio;
?>

<?php require '../../../config.php'; ?>
<?php require '../../../../lib/config.php'; ?>
<?php include INC_DIR.'header.php'; ?>

<!DOCTYPE HTML>
<html>
	<head>
		
	</head>
	<body>
        <?php require_once( '../../../../app/inc/navbars/navbar_esperto.php'); include_once("../../../../heatmap/heatmap_db.php"); ?>
        <?php 
            $ob_studio = ob_studio($id_studio); //chiamo funzione che effettua query per recuperare nome caso di studio
            $r = $db->sql_fetchrow($ob_studio);
    ?>
    
    <h2 align="center">Statistiche comportamento utenti dello studio: <?php echo $r['obiettivo'] ?></h2> <!--mostro a video il titolo dello studio corrente--> 
		<br />
		
		<div class="container">
			<br />
			<div class="panel panel-default">
        		 <div class="panel-heading"><b>Partecipanti</b></div>
    	 		 <div class="panel-body">
				 <form action="filter.php" method="post">
	
					<?php
			
						$servername = DB_HOST;
						$username = DB_USER;
						$password = DB_PASSWORD;
						$dbname = DB_NAME;
				
						// Create connection
						$conn = new mysqli($servername, $username, $password, $dbname);
						// Check connection
						if ($conn->connect_error) {
				    		die("Connection failed: " . $conn->connect_error);
						} 
				
						$sql = "SELECT DISTINCT us.username, smtr.client_id 
								FROM users us JOIN smt2_ass_task_users_records tur ON us.user_id = tur.id_user 
									JOIN task ON task.id_task = tur.id_task 
									JOIN smt2_records smtr ON tur.id_records = smtr.id 
									JOIN studio s ON task.id_studio =".$id_studio;
						
						$result = $conn->query($sql);
				
						if ($result->num_rows > 0) {
				    		echo '<table class="table table-hover"><tr><th>Username</th><th>Inizio studio</th><th>Fine studio</th><th>Azioni</th></tr><tbody>';
				    		// output data of each row
				    		while($row = $result->fetch_assoc()) {
				    	
								$start_date = "";
								$end_date = "";
								$i = TRUE;
						
								$sql_records = 'SELECT sess_date FROM smt2_records WHERE client_id = "'.$row["client_id"].'"';
								$res = $conn->query($sql_records);
						
								if ($res->num_rows > 0)
								{
									while($row_date = $res->fetch_assoc())
									{
										if($i == TRUE)
										{
											$start_date = $row_date["sess_date"];
											$i = FALSE;	
										}
								
										$end_date = $row_date["sess_date"];
									}
								}
						 		else {
				    				echo "0 results";
								}
					
								$start = startEndDateFormat($start_date)." 12:00 am";
								$end = startEndDateFormat($end_date)." 11:59 pm";
					
								echo '<tr><td>'
									.$row["username"].'</td>
									<td><input id="from" type="hidden" name="from" value="'.$start.'">'.$start_date.'</input></td>
									<td><input id="to" type="hidden" name="to" value="'.$end.'">'.$end_date.'</input></td>
									<td><button type="submit" class="btn btn-default btn-sm" name="client_id" value="'.$row["client_id"].'" checked="checked"><span class="glyphicon glyphicon-ice-lolly"></span>&nbsp;Visualizza statistiche</button>
						  		</td></tr>';
					
						}
                        echo '<tr><td><b>Tutti gli utenti</b></td>
                                <td></td>
                                <td></td>
                                <td><button type="submit" class="btn btn-default btn-sm" name="client_id" value="%" checked="checked"><span class="glyphicon glyphicon-ice-lolly"></span>&nbsp;Visualizza statistiche</button>
                            </td></tr>';
				    	echo "</tbody></table>";
					} else {
				    echo "0 results";
					}
					$conn->close();
				
					//Modifica records.sess_date nel formato mm/dd/yyyy
					function startEndDateFormat($value)
					{
						$value = substr($value, 0, 10);
					
						$value = date("m/d/Y", strtotime($value));
						
						return $value;
					}	
				?>
			
		</form>
		</div>		
		</div>
		</div>
		<?php //echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';?>		
	</body>
</html>
