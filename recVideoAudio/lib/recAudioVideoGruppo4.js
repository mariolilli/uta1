
var urlFull = '';

var flagUpload = 0;


var idTaskJS;
var idUserJS;
var idStudioJS;
var idFlagAudioJS;
var idFlagVideoJS;
var idURLfinaleJS;
var idDurataMaxJS;

function setSessionStorage(){
 idTaskJS = parseInt(sessionStorage.getItem("idTask"));
 idUserJS = parseInt(sessionStorage.getItem("idUser"));
 idStudioJS = parseInt(sessionStorage.getItem("idStudio"));
 idFlagAudioJS = parseInt(sessionStorage.getItem("flagAudio"));
 idFlagVideoJS = parseInt(sessionStorage.getItem("flagVideo"));
 idURLfinaleJS = sessionStorage.getItem("urlFinale");
 idDurataMaxJS = parseInt(sessionStorage.getItem("durataMax"));
}

var recordRTC = [];
var mediaStream = null;
function recordTaskAudioVideo(idFlagAudioJS, idFlagVideoJS) {
    console.log('Flag audio' + idFlagAudioJS + ' Flag video' + idFlagVideoJS);

function successCallback(stream) {
  // RecordRTC usage goes here

  var videoOptions = {
  recorderType: MediaStreamRecorder,
  mimeType: 'video/webm\;codecs=vp9'
  };

    var audioOptions = {
  type: 'audio'
  };

var options;

if(idFlagAudioJS)
{
  options = audioOptions;
}else if(idFlagVideoJS)
{
  options = videoOptions;
}

if(idFlagAudioJS || idFlagVideoJS)
{
  recordRTC = RecordRTC(stream, options);
  recordRTC.startRecording();
}else {
  return;
}

mediaStream = stream;
console.log("Registrazione iniziata");
}

function errorCallback(error) {
  console.log("Errore getUserMedia");
}

var mediaConstraints = { video: true, audio: true };

if(idFlagAudioJS || idFlagVideoJS)
{
navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
}

}

function stopVideoTask(status) {


if(status == "task")
{

  ferma();

if(idFlagAudioJS || idFlagVideoJS) {

recordRTC.stopRecording(function(url) {
  var blob = this.getBlob();
  uploadRecAudioVideo(blob);

  if(mediaStream) mediaStream.stop();

  });

}
else {
  if(preloading(status) !== 1)
  {
    modalStudioTerminato();
  }else {
    window.location.replace("/utassistant/app/partecipante/partecipante_studio.php");
  }
}

      $("#exitStudy").fadeOut(0);
      salva_dati_task();

}
else {
  if(preloading(status) !== 1)
  {
    modalStudioTerminato();
  }else {
    window.location.replace("/utassistant/app/partecipante/partecipante_studio.php");
  }
}
}

function preloading(status = "task")
{
  var element = document.getElementById("questionarioID");
  var element2 = document.getElementById("terminaStudioID");
  var element3 = document.getElementById("taskSux");

  $("#exitStudy").fadeOut(0);
  document.getElementById("exitStudy").innerHTML = "<a href='#'> <span class='h4' align='right' > Attendere... </span> </a>";
  $("#exitStudy").fadeIn(700);

  if(status == "task")
  {
  document.getElementById("infoNav").innerHTML = "";
  document.getElementById("taskID").innerHTML = "";
  document.getElementById("cronometro").innerHTML = "";
}

  if (element != null) {
      document.getElementById("questionarioID").innerHTML = "";
      flagUpload = 1;
  }
  if (element2 != null) {
      document.getElementById("terminaStudioID").innerHTML = "";
      flagUpload = 2;
  }

  if (element3 != null) {
      document.getElementById("taskSux").innerHTML = "";
      flagUpload = 1;
  }

  return flagUpload;
}
function salva_dati_task()
{
	var url_iframe = window.frames["iframe_id"].document.baseURI;

	$.ajax({
          url: '/utassistant/app/partecipante/partecipante_save_task.php',
          type: 'GET',
          data: 'azione=inserisci' + '&id_task=' + idTaskJS + '&id_user=' + idUserJS + '&id_studio=' + idStudioJS + '&url_raggiunta=' + url_iframe + '&timetask=' + timetask + '&url_finale=' + idURLfinaleJS + '&durata_max=' + idDurataMaxJS*60,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data);
          //alert(data);
     })
     .fail(function(){
          //alert(data);
     });
}

function uploadRecAudioVideo(recRTC) {
  uploadToServer(recRTC, function(progress, fileURL) {
                        if(progress === 'ended') {
                          if(preloading() !== 1)
                          {
                            modalStudioTerminato();
                          }
                          else
                          {
                            window.location.replace("/utassistant/app/partecipante/partecipante_studio.php");
                          }
                            return;
                        }
                    //    button.innerHTML = progress;
					console.log(progress);
                    });
}

function uploadToServer(recordRTC, callback) {
    Date.prototype.ddmmyyyy = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = this.getDate().toString();
        return (dd[1] ? dd : "0" + dd[0]) +"_"+(mm[1] ? mm : "0" + mm[0]) +"_"+yyyy; // padding
    };
    d = new Date();

    var blob = recordRTC instanceof Blob ? recordRTC : recordRTC.blob;
    var fileType = blob.type.split('/')[0] || 'audio';
  //  console.log(fileType);
    var task = idTaskJS;
    var dataFile = d.ddmmyyyy();
    var fileName = "TASKID"+task+"__"+dataFile+"__"+(Math.random() * 1000).toString().replace('.', '');
    console.log("Carico file " + fileName);

    var directory = '';
    var studio = 'IDstudio_' + idStudioJS + '/';
    var user = 'IDutente_' + idUserJS + '/';
    if (fileType === 'video') {
        fileName += '.webm';
    } else {
        fileName += '.wav';
    }
    var formData = new FormData();
    formData.append(fileType + '-filename', fileName);
    formData.append(fileType + '-blob', blob);
    formData.append('-studio', studio);
    formData.append('-user', user);


    callback('Uploading ' + fileType + ' recording to server.');

    makeXMLHttpRequest('/utassistant/recVideoAudio/save.php', formData, function (progress) {
        if (progress !== 'ended') {
            callback(progress);
            return;
       }
        if (fileType === 'video') {
            directory = 'recVideoAudio/video/' + studio + user;
        }
        if (fileType != 'video') {
            directory = 'recVideoAudio/audio/' + studio + user;
        }

    //    var initialURL = location.origin + "/" + directory;//"http://localhost/utassistant/recVideoAudio/" + directory;

        urlFull = directory + fileName;
        callback('ended', urlFull + fileName);
if(progress == 'ended')
{
        if (urlFull != null && fileType === 'video') {
            $.post("/utassistant/recVideoAudio/servizi.php",
                    {idTaskJS: idTaskJS, idUserJS: idUserJS, urlFullVideo: urlFull})
                  .done(function(data) {if(!data.success) console.log(data); else console.log("Path video Inserito")}).fail(function(data,status,error) { console.log(error + "Errore chiamata ajax")});
//function (data) {console.log("Path video: " + data);
        }

        if (urlFull != null && fileType != 'video') {
            $.post("/utassistant/recVideoAudio/servizi.php",
                    {idTaskJS: idTaskJS, idUserJS: idUserJS, urlFullAudio: urlFull})
                    .done(function(data) {if(!data.success) console.log(data); else console.log("Path audio Inserito")}).fail(function(data,status,error) { console.log(error + "Errore chiamata ajax")});
        }
      }

    });
}

function makeXMLHttpRequest(url, data, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
        //    callback('upload-ended');
         callback('ended');
        }
    };

    request.upload.onloadstart = function () {
        callback('Caricamento...');
    };

    request.upload.onprogress = function (event) {
        callback('Caricamento... ' + Math.round(event.loaded / event.total * 100) + "%");
    };

    request.upload.onload = function () {
        callback('_');
    };

    request.upload.onload = function () {
        callback('Caricamento completato');
    };

    request.upload.onerror = function (error) {
        callback('Caricamento Fallito');
        console.error('XMLHttpRequest failed', error);
    };

    request.upload.onabort = function (error) {
        callback('Caricamento interrotto');
        console.error('XMLHttpRequest aborted', error);
    };

    request.open('POST', url);
    request.send(data);
}

var ore = 0, minuti = 0, secondi = 0, decimi = 0;
var vis = "";
var stop = true;
var durata = 0;
var timetask = 0;


function avvia(durata_massima) {
	durata = durata_massima;
	if(stop == true) {
		stop = false;
		cronometro();
	}
	//alert(durata_massima);
}

function cronometro() {
	if(stop == false) {
		decimi++;
		if(decimi > 9) {
			decimi = 0;
			secondi++;
		}
		if(secondi > 59) {
			secondi = 0;
			minuti++;
		}
		if(minuti > 59) {
			minuti = 0;
			ore++;
		}
		if(minuti == durata) {
		//	alert("Tempo massimo raggiunto");
			ferma();
		//	azzera();
			modalInterrompiTask();
		}
		mostra();
		setTimeout("cronometro()", 100);
	}



}

function mostra() {
	if(ore < 10) vis = "00" + ":"; else vis = ore;
	if(minuti < 10) vis = vis + "0";
	vis = vis + minuti + ":";
	if(secondi < 10) vis = vis + "0";
	vis = vis + secondi + ":" + decimi;
	document.getElementById("vis").innerHTML = vis;
}

function ferma() {
	stop = true;
	timetask = (minuti * 60) + secondi;
}

function azzera() {
	if(stop == false) {
		stop = true;
	}
    ore = minuti = secondi = decimi = 0;
	vis = "";
	mostra();
}

function modalInterrompiTask(){
     $('#modalMessaggioTempoMassimo').modal('show');
}

function modalStudioTerminato(){
     $('#modalStudioTerminato').modal('show');
}
