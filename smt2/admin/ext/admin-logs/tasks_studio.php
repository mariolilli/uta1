<?php
// Start the session
session_start();
?>

<?php
//Identificativo dello studio da valutare
$id_studio = $_POST['idstudio'];
?>

<?php require '../../../config.php'; ?>
<?php include INC_DIR.'header.php'; ?>

<!DOCTYPE HTML>
<html>
	<head>
        <title>Heatmap Tasks</title>
	</head>
	<body>
      <?php require_once '../../../../app/inc/navbars/navbar_esperto.php'; ?>
		<h1 align="center">Heatmap</h1>
		<br />

		<div class="container">
			<br />
			<div class="panel panel-default">
        		 <div class="panel-heading"><b>Tasks</b></div>
    	 		 <div class="panel-body">
				 <form action="tasks_studio_2.php" method="post">

					<?php

						$servername = DB_HOST;
						$username = DB_USER;
						$password = DB_PASSWORD;
						$dbname = DB_NAME;

						// Create connection
						$conn = new mysqli($servername, $username, $password, $dbname);
						// Check connection
						if ($conn->connect_error) {
				    		die("Connection failed: " . $conn->connect_error);
						}

                        //query per recuperare info sui task relativi al caso di studio selezionato
						$sql = "SELECT obiettivo, id_task, url
                                FROM  task
                                WHERE id_studio =".$id_studio;

						$result = $conn->query($sql);
				        $vettore = array();
                     	echo '<table class="table table-hover"><tr><th>Obiettivo</th><th>URL Principale</th><th>Azioni</th></tr><tbody>';
                        while($row = $result->fetch_assoc()) {

                            echo '<tr><td>'
									.$row["obiettivo"].'</td>
                                    <td>'.$row["url"].'<input id="url_" type="hidden" name="url_" value="'.$row["url"].'"></input></td>
									<td><button type="submit" class="btn btn-default btn-sm" name="task_id" value="'.$row["id_task"].'" checked="checked"><span class="glyphicon glyphicon-fire"></span>&nbsp;Heatmap</button>
						  		</td></tr>';
                        }


				?>




		</form>
		</div>
		</div>
		</div>

	</body>
</html>
