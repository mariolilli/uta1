<?php
// server settings are required - relative path to smt2 root dir
require '../../../config.php';
require '../../../../lib/config.php';
// protect extension from being browsed by anyone
require SYS_DIR . 'logincheck.php';
// now you have access to all CMS API

// use ajax settings
require './includes/settings.php';

include INC_DIR . 'header.php';

// display a warning message for javascript-disabled browsers
echo check_noscript();

// check defaults from DB or current sesion
$show = (isset($_SESSION['limit']) && $_SESSION['limit'] > 0) ? $_SESSION['limit'] : db_option(TBL_PREFIX . TBL_CMS, "recordsPerTable");
// sanitize
if (! $show) {
    $show = $defaultNumRecords;
}

$id_studio = $_POST['idstudio'];
?>

<!-- <p><a href="./howto/">Some guides are available</a> to help you with these logs.</p> -->


<div class="container">
        <?php  require_once( '../../../../app/inc/navbars/navbar_esperto.php'); include_once("../../../../heatmap/heatmap_db.php"); ?>
            <?php
            $ob_studio = ob_studio($_POST['idstudio']); // chiamo funzione che effettua query per recuperare nome caso di studio
            $r = $db->sql_fetchrow($ob_studio);
            ?>
                <h2 align="center">Dettaglio delle statistiche sul comportamento utenti dello studio: <?php echo $r['obiettivo'] ?></h2>
	<!--mostro a video il titolo dello studio corrente-->
                <?php if (db_records()) { ?>

                    <!-- FILTRI -->
	<button type="button" class="btn btn-default" data-toggle="collapse"
		data-target="#demo">Filtra</button>
	<div id="demo" class="collapse">
		<br />

		<div class="well well-lg">

                            <?php check_notified_request("mine"); ?>

                                <form id="filter" class="center"
				action="filter.php" method="post">
				<legend>Filtra per</legend>
				<div class="row">

					<div class="col-sm-3">
                                            <?php echo select_domain(); ?>
                                        </div>
					<div class="col-sm-3">
                                            <?php echo select_cache(); ?>
                                        </div>

					<div class="col-sm-3">
                                            <?php echo select_tbl(TBL_PREFIX.TBL_OS, "os_id", "OS"); ?>
                                        </div>

					<div class="col-sm-3">
                                            <?php echo select_tbl(TBL_PREFIX.TBL_BROWSERS, "browser_id", "Browser"); ?>
                                        </div>
				</div>
				<br> <br>

				<legend>Raggruppa</legend>
				<div class="row">
					<div class="col-sm-4">
                                            <?=select_group()?>
                                        </div>
					<div class="col-sm-4">
                                            <?=select_records()?>
                                        </div>

				</div>
				<br> <br>

				<legend>Filtra risultati per data</legend>
				<div class="form-group">
					<div class="col-sm-6">
                                            <?=select_date("Da")?>
                                        </div>
					<div class="col-sm-6">
                                            <?=select_date("a")?>
                                        </div>
				</div>
				<br> <br> <br>

		</div>


		<input type="submit" class="btn btn-default" value="Applica filtro" />
		<input type="submit" name="reset" class="btn btn-default"
			value="Annulla filtro" />

                        <?php
                    /*
                     * // massive bulk function (not implemented)
                     * if (is_root() && isset($_SESSION['filterquery'])) {
                     * echo '<input type="submit" name="delete" class="button round delete conf" value="Delete filtered logs" />';
                     * }
                     */
                    ?>


                            <button type="button"
			class="btn btn-default" data-toggle="modal" data-target="#myModal">Esporta
			risultati</button>

		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Export</h4>
					</div>
					<div class="modal-body">
						<label for="csv">Format:</label> <label for="csv"
							class="radio-inline"> <input id="csv" type="radio" name="format"
							class="radio" value="csv" checked="checked" /> CSV
						</label> <label for="tsv" class="radio-inline"> <input id="tsv"
							type="radio" name="format" class="radio" value="tsv" /> TSV&nbsp;
						</label>

					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-default" name="download"
							value="Export logs" />
						<button type="button" class="btn btn-default" name="download"
							data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>

		<button type="button" class="btn btn-default" data-toggle="collapse"
			data-target="#demo">Chiudi</button>


                            <?php
                    /*
                     * if (!isset($_SESSION['filterquery'])) {
                     * echo checkbox("dumpdb", "Dump whole database");
                     * }
                     */
                    ?>
                                <!--
          <p class="left">
            <small>
              <sup>1</sup> each log is stored in a single file, then all are compressed in a ZIP file.
            <br />
              <sup>2</sup> all logs are dumped in a single file (logs are separated by a newline).
            </small>
          </p>
          -->


	</div>
	</form>
	<hr />
	<br />

                    <?php } ?>

                        <!-- Tabella con i rilevamenti del mouse -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Logs</b>
		</div>
		<div class="panel-body">

			<div id="records">
                                    <?php check_notified_request("records")?>

                                        <!-- //Modificato (Bootstrap: ho sostituito cms con table table-hover) -->
                                        <?php
                                        if ($_SESSION['groupby'] != 'id_task') {
                                            ?>

                                            <table
					class="table table-hover" cellpadding="10" cellspacing="1">
					<thead>
						<tr>
							<th>Titolo task</th>
							<th>ID dominio</th>
							<th>ID pagina</th>
							<th title="Format: yyyy/mm/dd">Data</th>
							<th title="In seconds">Tempo</th>
							<!--<th>interaction time</th>-->
							<th>Clicks</th>
							<!--
          <th>% moves</th>
          <th>% vscroll</th>
          -->
							<th>Note</th>
							<th>Azioni</th>
						</tr>
					</thead>
					<tbody>
                                                    <?php include './includes/tablerows.php'; ?>
                                                </tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- end table panel-default -->
                        <?php
                                        } else {
                                            ?>
                            <table class="table table-hover"
		cellpadding="10" cellspacing="1">
		<thead>
			<tr>
				<th>Titolo task</th>

				<th>#pages</th>
				<th>#notes</th>
				<th title="Format: yyyy/mm/dd">Inizio studio</th>
				<th title="Format: yyyy/mm/dd">Fine studio</th>
				<th title="In seconds">Tempo medio</th>
				<th>#clicks medio</th>

				<!--<th>interaction time</th>-->


				<th>action</th>
			</tr>
		</thead>
		<tbody>
                                    <?php include './includes/tableTask.php'; ?>
                                </tbody>
	</table>
</div>
</div>
</div>
<!-- end table panel-default -->


<?php
                                        }
                                        ?>


        <?php
        // the 'more' button
        if (! empty($displayMoreButton)) {
            echo '<a href="./?page=' . ++ $page . '&amp;' . $resetFlag . '" class="btn btn-default" id="more">' . $showMoreText . '</a>';
        } else {
            // echo $noMoreText;
            echo '
		  		<div class="alert alert-warning">
		     	   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			   	      <strong>Attenzione!</strong> Non ci sono altri risultati!.
		  		</div>
		  ';
        }

        // helper functions
        function checkbox($id, $label)
        {
            $select = (isset($_SESSION[$id])) ? 'checked="checked"' : null;
            $c = '<br /><br /><input type="checkbox" ' . $select . ' id="' . $id . '" name="' . $id . '" />';
            $c .= '&nbsp;<label for="' . $id . '" class="mr">' . $label . '</label>';

            return $c;
        }

        function select_tbl($table, $id, $label)
        {
            $s = '<label for="' . $id . '">' . $label . '</label> ';
            $s .= '<select id="' . $id . '" name="' . $id . '" class="form-control">';
            $s .= '<option value="">---</option>';
            $rows = db_select_all($table, "*", "1");
            foreach ($rows as $row) {
                $select = (isset($_SESSION[$id]) && $row['id'] == $_SESSION[$id]) ? 'selected="selected"' : null;
                $s .= '<option ' . $select . ' value="' . $row['id'] . '">' . $row['name'] . '</option>';
            }
            $s .= '</select>';
            return $s;
        }

        function select_date($id)
        {
            $d = '<label for="' . $id . '" class="ml">' . ucfirst($id) . '</label> ';
            $val = (! empty($_SESSION['filterquery']) && isset($_SESSION[$id])) ? $_SESSION[$id] : null;
            $d .= '<input type="text" id="' . $id . '" name="' . $id . '" class="form-control" value="' . $val . '" data-toggle="tooltip" title="Date format: MM/GG/AAAA hh:mm am/pm"/>';
            return $d;
        }

        function select_cache()
        {
            $s = '<label for="cache">Pagina</label> ';
            $s .= '<select id="cache" name="cache_id" class="form-control">';
            $s .= '<option value="">---</option>';
            // $rows = db_select_all(TBL_PREFIX.TBL_CACHE, "id, title", "1 ORDER BY id DESC");SELECT DISTINCT smt2_cache.id, smt2_cache.title FROM smt2_cache INNER JOIN smt2_records ON smt2_cache.id = smt2_records.cache_id WHERE smt2_records.client_id = 'ef0648e39f32bc04585f'
            $rows = db_query_custom("SELECT DISTINCT smt2_cache.id, smt2_cache.title FROM smt2_cache INNER JOIN smt2_records ON smt2_cache.id = smt2_records.cache_id INNER JOIN smt2_ass_task_users_records ON
              smt2_records.id = smt2_ass_task_users_records.id_records WHERE smt2_records.client_id LIKE '" . $_SESSION['client_id'] . "' and smt2_ass_task_users_records.id_task IN (select task.id_task FROM task INNER JOIN studio ON task.id_studio = studio.id_studio WHERE studio.id_studio = '".$_SESSION['idstudio']."')" );
            // pad with zeros the page id
            $num = db_select(TBL_PREFIX . TBL_CACHE, "MAX(id) as max", 1);
            $n = strlen($num['max']);
            foreach ($rows as $row) {
                $select = (isset($_SESSION['cache_id']) && $row['id'] == $_SESSION['cache_id']) ? 'selected="selected"' : null;
                $s .= '<option ' . $select . ' value="' . $row['id'] . '">' . pad_number($row['id'], $n) . ': ' . trim_text($row['title']) . '</option>';
            }
            $s .= '</select>';
            return $s;
        }

        function select_domain()
        {
            $s = '<label for="cache">Dominio</label> ';
            $s .= '<select id="domain" name="domain_id" class="form-control">';

            $s .= '<option value="">---</option>';
            // $rows = db_select_all(TBL_PREFIX.TBL_DOMAINS, "id, domain", "1 ORDER BY id DESC"); // GROUP BY domain?
            $rows = db_query_custom("SELECT DISTINCT smt2_domains.id, smt2_domains.domain FROM smt2_domains INNER JOIN smt2_records ON smt2_domains.id = smt2_records.domain_id INNER JOIN smt2_ass_task_users_records ON
              smt2_records.id = smt2_ass_task_users_records.id_records WHERE smt2_records.client_id LIKE '" . $_SESSION['client_id'] . "' and smt2_ass_task_users_records.id_task IN (select task.id_task FROM task INNER JOIN studio ON task.id_studio = studio.id_studio WHERE studio.id_studio = '".$_SESSION['idstudio']."')");
            // pad with zeros the domain id
            $num = db_select(TBL_PREFIX . TBL_DOMAINS, "MAX(id) as max", 1);
            $n = strlen($num['max']);
            foreach ($rows as $row) {
                $select = (isset($_SESSION['domain_id']) && $row['id'] == $_SESSION['domain_id']) ? 'selected="selected"' : null;
                $s .= '<option ' . $select . ' value="' . $row['id'] . '">' . pad_number($row['id'], $n) . ': ' . trim_chars($row['domain']) . '</option>';
            }
            $s .= '</select>';
            return $s;
        }

        function select_client()
        {
            $s = '<label for="client">Browser ID</label> ';
            $s .= '<select id="client" name="client_id" class="form-control">';
            $s .= '<option value="">---</option>';
            $rows = db_select_all(TBL_PREFIX . TBL_RECORDS, "DISTINCT client_id", "1");
            foreach ($rows as $row) {
                $select = (isset($_SESSION['client_id']) && $row['client_id'] == $_SESSION['client_id']) ? 'selected="selected"' : null;
                $s .= '<option ' . $select . ' value="' . $row['client_id'] . '">' . mask_client($row['client_id']) . '</option>';
            }
            $s .= '</select>';
            return $s;
        }

        function select_fps()
        {
            $s = '<label for="fps">FPS</label> ';
            $s .= '<select id="fps" name="fps" class="form-control">';
            $s .= '<option value="">---</option>';
            $rows = db_select_all(TBL_PREFIX . TBL_RECORDS, "DISTINCT fps", "1");
            foreach ($rows as $row) {
                $select = (isset($_SESSION['fps']) && $row['fps'] == $_SESSION['fps']) ? 'selected="selected"' : null;
                $s .= '<option ' . $select . ' value="' . $row['fps'] . '">' . $row['fps'] . '</option>';
            }
            $s .= '</select>';
            return $s;
        }

        function select_group()
        {
            $s = '<label for="groupby">Raggruppa risultati per</label> ';
            $s .= '<select id="groupby" name="groupby" class="form-control">';
            $s .= '<option value="">---</option>';

            if (($_SESSION['client_id']) == '%') {
                $opt = array(
                    "id_task" => "Task"
                )
                // "client_id" => "Client",
                // "cache_id" => "Page",
                // "domain_id" => "Domain",
                // "ip" => "Location"
                ;
            } else {
                $flag = FALSE;
                $opt = array(
                    "id_task" => "Task"
                )
                // "client_id" => "Client",
                // "cache_id" => "Page",
                // "domain_id" => "Domain",
                // "ip" => "Location",
                ;
            }
            foreach ($opt as $key => $val) {
                $select = (! empty($_SESSION['groupby']) && $key == $_SESSION['groupby']) ? 'selected="selected"' : null;
                $s .= '<option ' . $select . ' value="' . $key . '">' . $val . '</option>';
            }
            $s .= '</select>';
            return $s;
        }

        function select_records()
        {
            $s = '<label for="limit">Numero di righe in tabella</label> ';
            $s .= '<select id="limit" name="limit" class="form-control">';
            $s .= '<option value="">---</option>';
            $num = array(
                10,
                50,
                100,
                200,
                500,
                1000
            );
            foreach ($num as $n) {
                $select = (! empty($_SESSION['limit']) && $n == $_SESSION['limit']) ? 'selected="selected"' : null;
                $s .= '<option ' . $select . ' value="' . $n . '">' . $n . '</option>';
            }
            $s .= '</select>';
            return $s;
        }

        function input_time($id)
        {
            $value = (! empty($_SESSION[$id]) && strlen($_SESSION[$id]) < 5) ? $_SESSION[$id] : "0";
            $c = '<label for="' . $id . '" class="ml">' . $id . '</label> ';
            $c .= '<input type="text" class="text" size="2" id="' . $id . '" name="' . $id . '" value="' . $value . '" />';
            return $c;
        }
        ?>

</div>
<!-- end container table -->


<script type="text/javascript" src="<?=ADMIN_PATH?>js/jquery.stripy.js"></script>
<script type="text/javascript"
	src="<?=ADMIN_PATH?>js/jquery.tablesorter.min.js"></script>
<script type="text/javascript"
	src="<?=ADMIN_PATH?>js/jquery.tablesorter.widgets.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_PATH?>js/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="<?=ADMIN_PATH?>js/jquery.ui.datepicker.js"></script>
<script type="text/javascript"
	src="<?=ADMIN_PATH?>js/jquery.ui.slider.js"></script>
<script type="text/javascript"
	src="<?=ADMIN_PATH?>js/jquery.ui.timepicker.js"></script>
<script type="text/javascript">
                //<![CDATA[
                $(function () {

                    // shorcut for jQuery selectors
                    var legends = "fieldset legend";
                    var records = "#records table";

                    // shorcut to (smt)2 aux functions
                    var aux = window.smt2fn;
                    // check saved cookie
                    var cookieId = "smt-hiddenFieldsets";
                    if (aux.cookies.checkCookie(cookieId)) {
                        var hide = aux.cookies.getCookie(cookieId).split(",");
                        for (var i = 0; i < hide.length; ++i) {
                            $(legends).eq(hide[i]).nextAll().toggle();
                        }
                    }
                    // save routine
                    function savePos() {
                        var hideElems = [];
                        $(legends).attr('title', "Toggle fieldset").each(function (i, val) {
                            if ($(this).nextAll().is(':hidden')) {
                                hideElems.push(i);
                            }
                        });
                        aux.cookies.setCookie(cookieId, hideElems, 30);
                    };

                    // click behaviour
                    $(legends).attr('title', "toggle fieldset").css('cursor', "pointer").click(function () {
                        var elems = $(this).nextAll();
                        elems.toggle();
                        savePos();
                    });

                    // display nice table
                    $(records).tablesorter({
                        widgets: ['zebra', 'columns'],
                        //usNumberFormat : false,
                        //sortReset      : true,
                        //sortRestart    : true,
                        headers: {
                            8: {
                                sorter: false
                            }
                        },
                        cssHeader: "headerNormal"
                    });

                    // date picker UI widget
                    $('.datetime').datepicker({
                        duration: '',
                        showTime: true,
                        constrainInput: false,
                        beforeShow: function (i, e) {
                            e.dpDiv.css('z-index', aux.getNextHighestDepth());
                        }
                    });

                    // slider UI widget
                    var sliderElem = $('#slider-range');
                    var minInput = $('input#mintime');
                    var maxInput = $('input#maxtime');

                    function formatSlider(arrValues) {
                        $("#slider-amount").html('min. ' + arrValues[0] + ' &mdash; max. ' + arrValues[1]);
                    };

                    <?php
                    $time = db_select(TBL_PREFIX . TBL_RECORDS, "MAX(sess_time) as max", 1);
                    $maxTime = ceil($time['max']);
                    if (! isset($_SESSION['filterquery'])) {
                        ?>
                    // set time range
                    minInput.val(0);
                    maxInput.val(<?=$maxTime?>);
                    <?php
                    }
                    ?>
                    // hide regular input fields
                    sliderElem.find("input,label").hide();
                    // a silly check before configuring the time slider
                    if (maxInput.val() == 0) {
                        maxInput.val(<?=$maxTime?>);
                    }
                    sliderElem.slider({
                        range: true,
                        min: 0,
                        max: <?=$maxTime?>,
                        //step: 5,
                        values: [minInput.val(), maxInput.val()],
                        slide: function (event, ui) {
                            formatSlider(ui.values);
                        },
                        stop: function (event, ui) {
                            minInput.val(ui.values[0]);
                            maxInput.val(ui.values[1]);
                        }
                    });
                    formatSlider(sliderElem.slider("values"));

                    // append more records to main table (see include/settings.php)
                    var page = <?=$page?>;
                    var show = <?=$show?>;
                    var more = $('a#more');
                    more.click(function (e) {
                        // remove focus
                        $(this).blur();
                        // async request
                        $.get('includes/tablerows.php?page=' + page + '&show=' + show, function (data) {
                            $(records + ' tbody').append(data);
                            $(records).stripy().trigger("update");
                            // update CMS links, delete buttons, etc.
                            SetupCMS.all();
                            // increment page counter
                            ++page;
                            // remove the 'more' link if there are no more records
                            var r = new RegExp('<?=$noMoreText?>');
                            var s = data.search(r);
                            if (s != -1) {
                                more.parent().append('<?=$noMoreText?>');
                                more.remove();
                            }
                        });
                        // cancel default action
                        e.preventDefault();
                    });

                });
                //]]>
            </script>

<!-- plug-in tooltip bootstrap -->
<script>
                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>
<?php //  echo '<pre>' . print_r($_POST, TRUE) . '</pre>';?>
 <?php //  echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';?>
            <?php include INC_DIR.'footer.php'; ?>
