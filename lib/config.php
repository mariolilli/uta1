<?php
if (is_dir("install/")) {
    header("Location: install/");
    die();
}

// The following line appeases recent versions of PHP. If you want a different time zone,
// insert a similar call into models/settings.php with your desired time zone
// NOTE: You probably do want a different time zone.
date_default_timezone_set('UTC');


require_once (dirname(__FILE__) . "/settings.php");

// Dbal Support - Thanks phpBB ; )
require_once (dirname(__FILE__) . "/db/" . $dbtype . ".php");

// Construct a db instance
$db = new dbal_mysqli();
if (is_array($db->sql_connect($db_host, $db_user, $db_pass, $db_name, $db_port, false, false))) {
    die("Unable to connect to the database");
}

if (! isset($language))
    $langauge = "en";

require_once (dirname(__FILE__) . "/lang/" . $langauge . ".php");
require_once (dirname(__FILE__) . "/class.user.php");
require_once (dirname(__FILE__) . "/class.mail.php");
require_once (dirname(__FILE__) . "/funcs.user.php");
require_once (dirname(__FILE__) . "/funcs.general.php");
require_once (dirname(__FILE__) . "/class.newuser.php");
require_once (dirname(__FILE__) . "/class.newuserpart.php");

session_start();

// -------------------------DEFINISCO COSTANTE PER PATH RELATIVO------------------------------------------------------------

//---Path assoluti--//
define('BASE_DIR', "/utassistant/"); //root
define('APP_DIR', BASE_DIR."app/"); // /utassistant/app/
define('PARTECIPANTE_DIR', APP_DIR."partecipante/");// /utassistant/app/partecipante
define('INCLUDE_PARTECIPANTE_DIR', "app/partecipante/");// relativo
define('ESPERTO_DIR', APP_DIR."esperto/"); // /utassistant/app/esperto
define('ACCOUNT_DIR', APP_DIR."account/");// /utassistant/app/account

define('INCLUDE_ESPERTO_DIR', "app/esperto/");// relativo
define('INCLUDE_DIR', BASE_DIR."app/inc/");// relativo controllare
define('INCLUDE_NAVBAR_DIR', INCLUDE_DIR."navbars/"); // relativo controllare

define('INCLUDE_QUESTIONARI_LIB_DIR', "questionari/lib/");// relativo controllare

define('NAVBAR_DIR', BASE_DIR."app/inc/navbars/"); // uguale a INCLUDE_NAVBAR_DIR


define('QUESTIONARI_DIR', "questionari/");// relativo controllare
define('SUS_DIR', BASE_DIR.QUESTIONARI_DIR."sus/"); // assoluto controllare
define('ATTRAKDIFF_DIR', BASE_DIR.QUESTIONARI_DIR."attrakdiff/");// relativo controllare
define('NASATLX_DIR', BASE_DIR.QUESTIONARI_DIR."nasatlx/");// assoluto controllare
define('NPS_DIR', BASE_DIR.QUESTIONARI_DIR."nps/");// relativo controllare
define('UMUX_DIR', BASE_DIR.QUESTIONARI_DIR."umux/");// relativo controllare
define('LIB_SUS_DIR', SUS_DIR."lib/");// assoluto controllare
define('LIB_ATTRAKDIFF_DIR', BASE_DIR."questionari/attrakdiff/lib/");// assoluto controllare
define('LIB_NASATLX_DIR', NASATLX_DIR."lib/");// assoluto controllare
define('LIB_NPS_DIR', NPS_DIR."lib/");// assoluto controllare
define('LIB_UMUX_DIR', UMUX_DIR."lib/");// assoluto controllare
define('CONTENT_DIR', BASE_DIR."content/");// assoluto controllare
define('JS_DIR', CONTENT_DIR."js/");// assoluto controllare
define('CSS_DIR', CONTENT_DIR."css/");// assoluto controllare
define('IMG_DIR', CONTENT_DIR."img/");// assoluto controllare
define('CLICKMAP_DIR', BASE_DIR."clickmap/");// assoluto controllare
define('LIB_CLICKMAP_DIR', CLICKMAP_DIR."lib/");// assoluto controllare
define('RECVIDEOAUDIO_DIR', BASE_DIR."recVideoAudio/");// assoluto controllare
define('LIB_RECVIDEOAUDIO_DIR', RECVIDEOAUDIO_DIR."lib/");// assoluto controllare
// -------------------------------------------------------------------------------------------------------------
                                      
// Global User Object Var
                                      // loggedInUser can be used globally if constructed
if (isset($_SESSION["userPieUser"]) && is_object($_SESSION["userPieUser"]))
    $loggedInUser = $_SESSION["userPieUser"];
else 
    if (isset($_COOKIE["userPieUser"])) {
        $db->sql_query("SELECT session_data FROM " . $db_table_prefix . "sessions WHERE session_id = '" . $_COOKIE['userPieUser'] . "'");
        $dbRes = $db->sql_fetchrowset();
        if (empty($dbRes)) {
            $loggedInUser = NULL;
            setcookie("userPieUser", "", - parseLength($remember_me_length));
        } else {
            $obj = $dbRes[0];
            $loggedInUser = unserialize($obj["session_data"]);
        }
    } else {
        $db->sql_query("DELETE FROM " . $db_table_prefix . "sessions WHERE " . time() . " >= (session_start+" . parseLength($remember_me_length) . ")");
        $loggedInUser = NULL;
    }

?>
