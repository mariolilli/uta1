
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Robots" content="noindex, nofollow" />

  <?php if(!isset($_SESSION['heatmap'])){ ?> <!-- se non è settata la variabile heatmap stampo titolo e icona header relativo a smt2 -->
   
  <?php } ?>

  <script type="text/javascript" src="<?=ADMIN_PATH?>js/jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="<?=ADMIN_PATH?>js/flashdetect.min.js"></script>
  <script type="text/javascript" src="<?=ADMIN_PATH?>js/setupcms.js"></script>
  <script type="text/javascript" src="<?=SMT_AUX?>"></script>
  
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  <?php
  // check custom headers
  if (count($_headAdded) > 0)
  {
    foreach ($_headAdded as $tag)
    {
      echo $tag.PHP_EOL;
    }
  }
  ?>
