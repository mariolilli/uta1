<?php
require_once '../lib/config.php';

$sql_query = "";
$result_array = array();

if (is_ajax()) {
    try {

        if (! isset($_POST['idstudio']))
            throw new Exception('Errore invio POST idstudio.');

        if (! isset($_POST['tipomedia']))
            throw new Exception('Errore invio POST tipomedia.');

        switch ($_POST['tipomedia']) {
            case "audio":
                $sql_query = "SELECT username, task.obiettivo, asu.data_completamento, file.path FROM file_audio as file
    INNER JOIN task ON task.id_task = file.task_id
    INNER JOIN studio ON task.id_studio = studio.id_studio
    INNER JOIN users as usr ON file.user_id = usr.user_id
    INNER JOIN ass_studio_users as asu ON usr.user_id = asu.id_utente
    WHERE file.task_id IN (SELECT task.id_task FROM task INNER JOIN studio ON task.id_studio = studio.id_studio WHERE studio.id_studio = ".$_POST['idstudio'].") AND asu.flag_completato = 1 AND studio.flag_audio = 1 AND asu.id_studio = ".$_POST['idstudio'];
                break;

            case "video":
                $sql_query = "SELECT username, task.obiettivo, asu.data_completamento, file.path FROM file_video as file
    INNER JOIN task ON task.id_task = file.task_id
    INNER JOIN studio ON task.id_studio = studio.id_studio
    INNER JOIN users as usr ON file.user_id = usr.user_id
    INNER JOIN ass_studio_users as asu ON usr.user_id = asu.id_utente
    WHERE file.task_id IN (SELECT task.id_task FROM task INNER JOIN studio ON task.id_studio = studio.id_studio WHERE studio.id_studio = ".$_POST['idstudio'].") AND asu.flag_completato = 1 AND studio.flag_video = 1 AND asu.id_studio = ".$_POST['idstudio'];
                break;
        }

        $result = $db->sql_query($sql_query);

    //    $result_array = $db->sql_fetchall($result);
        while($r = $db->sql_fetchrow($result)) {
            $result_array['data'][] = $r;
        }


        $json = json_encode($result_array);

        echo $json;
    } catch (Exception $ex) {
        echo json_encode(array(
            'success' => false,
            'reason' => $ex->getMessage()
        ));
    }
}

function is_ajax()
{
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}
