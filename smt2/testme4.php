<?php
// Start the session
session_start();
?>


<!DOCTYPE html>
<!-- This page is a bulletproof example to test smt2 recording capabilities. -->
<html>
<HEAD>
   <TITLE>Esempio 3</TITLE>
     <?php /*

    //Crea i cookie
    $cookie_name = "id_user";
    $cookie_value = $_SESSION["id_user"];

    setcookie($cookie_name, $cookie_value, time() + (86400 * 30),"/");

    $cookie_name = "id_task";
    $cookie_value = $_SESSION["idtask"];

    setcookie($cookie_name, $cookie_value, time() + (86400 * 30),"/");
	
	
    //Controllo se il tracciamento del mouse è abilitato
    if(isset($_SESSION["flag_comportamento"]))
	{
		$comportamento = $_SESSION["flag_comportamento"];

		if($comportamento == 1)
		{
			echo '<script type="text/javascript" src="http://localhost/userpie/smt2/core/js/smt2e.min.js"></script>
			      <script type="text/javascript">
  				try 
  				{
    				smt2.record({ 
      				warn:true, 
      				warnText:"smt2e is going to track your cursor activity."});
  				} catch(err) {}
  			</script>';
		}
	} */
  ?>
</HEAD>
<BODY BACKGROUND="titto_green_paper.jpg">

<CENTER><H1>Esempio 3: uso di tabelle e di un contatore</H1></CENTER>

<UL> <!-- a questa UL (unordered list) non corrispondera' nessun 
          LI (list item), il suo scopo e' solo quello di indentare
          il testo della pagina -->

<P>    <!-- P sta per paragraph, impone di saltare una linea -->
<FONT SIZE=+2>esempio di tabella:</FONT>
<P>
<CENTER><TABLE BORDER COLS=2 WIDTH="90%">
        <TR>
            <TD>Riga 1 - Colonna 1</TD>
            <TD>Una tabella pu&ograve; contenere qualsiasi cosa. 
                Questa che segue &egrave;
                una lista.
                <P>
                <UL>
                    <LI>elemento 1</LI>
                    <LI>elemento 2</LI>
               </UL>
           </TD>
       </TR>

       <TR>
           <TD>La tabella pu&ograve; contenere altre tabelle:<P>
               <TABLE BORDER COLS=2 WIDTH="100%">
                 <TR>
                     <TD>seconda tabella elem. (1,1)</TD>
                     <TD>seconda tabella elem. (1,2)</TD>
                 </TR>
               </TABLE><P>
           </TD>
           <TD>La tabella pu&ograve; contenere immagini&nbsp;
               <IMG SRC="dragon.gif" HEIGHT=50 WIDTH=66>
               questa è un'immagine GIF animato, il cui
               colore di sfondo è il trasparente.
           </TD>
      </TR>
      </TABLE>
</CENTER>

<P>
<FONT SIZE=+2>i bordi della tabella possono essere invisibili:</FONT>
<P>
<CENTER><TABLE COLS=2 WIDTH="50%">
        <TR>
            <TD>Riga 1 - Colonna 1: questo &egrave; un esempio di tabella i
                cui bordi sono invisibili
            </TD>
            <TD>Riga 1 - Colonna 2: il testo viene posizionato all'interno 
                di ogni casella, rispettando i mutui rapporti tra le 
                caselle
            </TD>
        </TR>
        <TR>
            <TD>Riga 2 - Colonna 1: questo sistema pu&ograve; essere usato per
                ottenere effetti di formattazione (allineamenti, 
                incolonnamenti) altrimenti impossibili per un linguaggio 
                che fa uso di marcatori logici e non fisici.
            </TD>
            <TD><CENTER>questo</CENTER>
                <CENTER>testo</CENTER>
                <CENTER>&egrave;</CENTER>
                <CENTER>centrato</CENTER>
            </TD>
       </TR>
       </TABLE>
</CENTER>

<HR WIDTH="100%">  <!-- HR = hard line -->
<BR><FONT SIZE=+2>questa pagina &egrave; stata acceduta&nbsp;
    200 volte.</FONT>

</UL> <!-- fine dell'indentatura -->
<p>Go to new page: <a href="http://localhost/userpie/smt2/testme5.php">TESTME5</a></p>

</body>
</html>

</BODY>
</HTML>


