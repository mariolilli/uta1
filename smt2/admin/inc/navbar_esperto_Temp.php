<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Logo -->
    <div class="navbar-header">
      <div>
        <a class="navbar-brand">
          <strong class="h2"> UTAssistant </strong>
        </a>
      </div>

        <div>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
    </div>

    <!--Voci del menu -->
    <div class="collapse navbar-collapse" id="mainNavBar">
        
        <!-- OBIETTIVO E/O DESCRIZIONE TASK SU NAVBAR HEATMAP -->
        <ul class="nav navbar-nav">
    <!-- inserisco obiettivo e descrizione task a sinistra del logo UTASSISTANT se è stata definita la variabile di sessione -->
     <?php if( isset($_SESSION['ob'])) { ?>
          <li id="infoNav">
           <a href="#" style="pointer-events: none; cursor: default;">
                <span class="h6"><strong><u>Obiettivo</u>: </strong><?php echo $_SESSION['ob']; ?><!--<strong><u><br>Descrizione</u>: </strong><?php // echo $_SESSION['ist']; ?> --> &nbsp;&nbsp;&nbsp;
               <!-- <span class="glyphicon glyphicon-info-sign"
                      data-toggle="modal" data-target="#myModal"><strong>Info</strong></span> --></span>
            </a>
            </li>  
        <?php } ?>
    </ul>
        
      <ul class="nav navbar-nav navbar-right">
        
         <li>
           <!--<a href="http://localhost/utassistant/index.php">--> <!--PERCORSO ASSOLUTO -->
              <a href="<?php echo BASE_DIR; ?>index.php"> <!-- PERCORSO RELATIVO -->
              <span class="h4">
                <span class="glyphicon glyphicon-home"></span>&nbsp;Home</span>
          </a>
        </li>
          
        <li>
           <!--<a href="http://localhost/utassistant/esperto_crea_studio.php">--> <!--PERCORSO ASSOLUTO -->
              <a href="<?php echo BASE_DIR; ?>esperto_crea_studio.php">  <!-- PERCORSO RELATIVO -->
              <span class="h4"><span class="glyphicon glyphicon-file"></span>&nbsp;Crea Studio</span>
          </a>
        </li>
<!-- AGGIUNGO CAMBIO PASSWORD PER MANTENERE UGUALE LA NAVBAR A QUELLA PRINCIPALE DEL SISTEMA -->
         <li>
           <!--<a href="http://localhost/utassistant/change-password.php">--> <!--PERCORSO ASSOLUTO -->
              <a href="<?php echo BASE_DIR; ?>change-password.php">  <!-- PERCORSO RELATIVO -->
              <span class="h4"><span class="glyphicon glyphicon-sort"></span>&nbsp;Cambia Password</span>
          </a>
        </li>
          
<!-- AGGIUNGO ICONE PER TRACCIABILITA' HEATMAP  BASANDOMI SU VARIABILI DI SESSIONE -->
<!-- @@@@@@@@@@@@@@@@@@@@ ELIMINATO @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
          <?php 
          /*
          if (isset($_SESSION['task_id'])) {?>
            <li>
          <a href="http://localhost/utassistant/tasks_studio.php">
              <span class="h4">
                <span class="glyphicon glyphicon-list"></span>Tasks</span>
          </a>
        </li>  
         <?php } ?>
          
          <?php if ($_SESSION['pagine']==1) { ?>
        <li>
          <a href="http://localhost/utassistant/tasks_studio_2.php">
              <span class="h4">
                <span class="glyphicon glyphicon-list-alt"></span>Pagine</span>
          </a>
        </li> 
    
        <?php } */?>
          
          
        <li>
         <!--<a href="http://localhost/utassistant/logout.php">--> <!--PERCORSO ASSOLUTO -->
              <a href="<?php echo BASE_DIR; ?>logout.php">  <!-- PERCORSO RELATIVO -->
              <span class="h4">
                <span class="glyphicon glyphicon-off"></span>&nbsp;Logout</span>
          </a>
        </li>
          
      </ul>
    </div>
       <?php if ($_SESSION['pagine']==1) { ?> <!-- compare solo nella pagina di visualizzazione dell'heatmap -->
       <!--<div class="options">
        <label>Raggio </label><input type="range" id="radius" value="25" min="10" max="50"  /><br />
        <label>Sfocatura </label><input type="range" id="blur" value="15" min="10" max="50" />
        </div> -->
        <!-- ALLINEO RAGGIO E SFOCATURA SULLA STESSA LINEA -->
        <div class="options"  style="float:left;">
        <label>Raggio </label><input type="range" id="radius" value="25" min="10" max="50"  />
        </div> 
        <div class="options"  style="float:left;">
        <label>Sfocatura </label><input type="range" id="blur" value="15" min="10" max="50" />
        </div> 
       <?php } ?>
    </div>
</nav>
