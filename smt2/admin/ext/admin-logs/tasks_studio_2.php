<?php
// Start the session
session_start();
?>

<?php
$id_task = $_POST['task_id']; //Recupero Identificativo del task su cui lavorare
$url = $_POST['url_'];        //Recupero url del task
//echo $id_task;
//echo $url;
?>

<?php require '../../../config.php'; ?>
<?php include INC_DIR.'header.php'; ?>

<!DOCTYPE HTML>
<html>
	<head>
        <title>Heatmap Tasks 2</title>
	</head>
	<body>
      
		<h1 align="center">Heatmap</h1>
		<br />
		
		<div class="container">
			<br />
			<div class="panel panel-default">
        		 <div class="panel-heading"><b>Pagine aperte durante il task</b></div>
    	 		 <div class="panel-body">
				 <form action="heatmap_studio.php" method="post">
	
					<?php
			
						$servername = DB_HOST;
						$username = DB_USER;
						$password = DB_PASSWORD;
						$dbname = DB_NAME;
				
						// Create connection
						$conn = new mysqli($servername, $username, $password, $dbname);
						// Check connection
						if ($conn->connect_error) {
				    		die("Connection failed: " . $conn->connect_error);
						} 
				
                        //query per recuperare pagine visitate nel task
						$sql = "SELECT DISTINCT smt2_cache.url 
                                from smt2_cache
                                join smt2_records
                                on smt2_cache.id = smt2_records.cache_id
                                join smt2_ass_task_users_records
                                on smt2_ass_task_users_records.id_records = smt2_records.id
                                where smt2_ass_task_users_records.id_task =".$id_task;

						$result = $conn->query($sql);
				        $vettore = array();
                     	echo '<table class="table table-hover"><tr><th>Url</th><th>Visualizzazione</th></tr><tbody>';
                        while($row = $result->fetch_assoc()) {
                            
                            //visualizzo in tabella le pagine visualizzate inviando al click i dati sull'url e task corrispondente
                            echo '<tr>
                                    <td>'
									.$row["url"].'
                                    </td>
                                    <td>
                                    <button type="submit" class="btn btn-default btn-sm" name="url_" value="'.$row["url"].'" checked="checked"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;Display</button>
                                    </td>
                                    <td>
                                    <input id="taskid" type="hidden" name="taskid" value="'.$id_task.'"hidden></input> 
                                    </td>
                                    </tr>';
                                   
                        }
                            
                             
				?>
			
    
                     
                     
		</form>
		</div>		
		</div>
		</div>
 
	</body>
</html>
