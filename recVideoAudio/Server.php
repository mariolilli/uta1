<?php

require_once './query.php';

class Server {

    private static $conn;
    private $c;
    private $allVideo = array();
    private $dbHost;
    private $dbUser;
    private $dbPass;
    private $dbPort;
    private $dbName;
    private $idStudios;
    private $JsonDati = array('dati' => ['datiV' => "", "string" => 'test']);


    private function __construct() {
        $this->DbConnect();
    }

    public static function Connessione() {
        if (static::$conn === NULL) {

            static::$conn = new Server();
        }
        return static::$conn;
    }

    /**
     * connessione al DB con PDO
     * @return boolean
     */
    protected function DbConnect() {
        $this->c = new PDO('mysql:host=localhost;dbname=utassistantdb;charset=utf8', 'root', '') OR die("Unable to connect to the database");
        return TRUE;
    }

    /**
     *
     * @param type $sql
     * @return string
     * Recupero tutti i task di uno studio
     */
    public function ShowTask($sql) {
        $category = '<option value="0">Scegli...</option>';
        foreach ($this->c->query($sql) as $row) {
            $category .= '<option value="' . $row['id_task'] . '">' . $row['obiettivo'] . '</option>';
        }
        return $category;
    }

    /**
     * Recupero tutti i task fatti da un utente
     * @param type $sql
     * @return string
     */
    public function ShowTaskU($sql) {
        $category = '<option value="0">Scegli...</option>';
        foreach ($this->c->query($sql) as $row) {
            $category .= '<option value="' . $row['id_task'] . '">' . $row['obiettivo'] . '</option>';
        }
        return $category;
    }

    /**
     * Recupero tutti gli utenti dello studio
     * @param type $sql
     * @return string
     */
    public function ShowUtenti($sql) {
        $category = '<option value="0">Scegli...</option>';
        foreach ($this->c->query($sql) as $row) {
            $category .= '<option value="' . $row['user_id'] . '">username:' . $row['username'] . ' email:' . $row['email'] . '</option>';
        }
        return $category;
    }

    /**
     * Recupero gli utenti di un determinato task
     * @param type $sql
     * @return string
     */
    public function ShowUtentiT($sql) {
        $category = '<option value="0">Scegli...</option>';
        foreach ($this->c->query($sql) as $row) {
            $category .= '<option value="' . $row['user_id'] . '">username:' . $row['username'] . ' email:' . $row['email'] . '</option>';
        }
        return $category;
    }

    /**
     * Recupero i video dei test
     * @param type $sql
     * @return string
     */
    public function loadVideo($sql) {
        $category = '<option value="0" disabled>Scegli...</option>';
        foreach ($this->c->query($sql) as $row) {
            $nomeV = explode("/", $row['path']);
            $category .= '<option value="' . $row['path'] . '">Nome file: ' . end($nomeV) . '</option>';
        }
        return $category;
    }

    /**
     * Recupero gli audio dei test
     * @param type $sql
     * @return string
     */
    public function loadAudio($sql) {
        $category = '<option value="0">Scegli...</option>';
        foreach ($this->c->query($sql) as $row) {
            $nomeA = explode("/", $row['path']);
            $category .= '<option value="' . $row['path'] . '">Nome file: ' . end($nomeA) . '</option>';
        }
        return $category;
    }

    /**
     *
     * @param type $url
     * @return boolean
     * Aggiugo la path di un nuovo audio riferito ad un task e utente
     */
    public function addAudio($sql) {
        try {
            $this->c->query($sql);
            echo "Path audio aggiunto";
        }catch (PDOException $e) {
            echo "Path audio non aggiunto";//$e->getMessage();
        }

    }

    /**
     *
     * @param type $url
     * @return boolean
     * Aggiungo la path di un nuovo video riferito ad un task e utente
     * usare
     */
    public function addVideo($sql) {
        try {
            $this->c->query($sql);
            echo "Path video aggiunto";
        }catch (PDOException $e) {
            echo "Path video non aggiunto";//$e->getMessage();
        }
    }

    /**
     *
     * @return type
     * carico tutti i video e audio di uno studio, task e utente particolare
     */
    public function LoadVideoAudio($sqlAudio, $sqlVideo) {
        $category = '<option value="0">Scegli..prova.</option>';
        $arrIDAudio = array();
        $arrPathAudio = array();
        $i = 0;
        foreach ($this->c->query($sqlAudio) as $row) {
            array_push($arrIDAudio, $row['id']);
            array_push($arrPathAudio, $row['path']);
            $i++;
        }
        $i = 0;
        foreach ($this->c->query($sqlVideo) as $row) {
            $category .= "<option value='" . $row['id'] . "'>id Video:'" . $row['id'] . "' id Audio:'" . $arrIDAudio[$i] . "'</option>";
            $i++;
        }
        return $category;
    }

    public function getPVideo() {
        return $this->pathVideo[0];
    }

}
?>
