/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */






$(document).ready(function () {
    var task = false, utenti = false, verifica = false;
    var scegli = '<option value="0">Scegli...</option>';
    var attendere = '<option value="0">Attendere...</option>';
    var co = 0;
    var Task = "1";
    var Utenti = "1";
    var a = JSON.parse(sessionStorage.getItem('idStudio'));
    var Studio = a[0];
    sessionStorage.setItem('DStudio', Task);
    $("select#videoq").html(scegli);
    $("select#audio").html(scegli);
    $("select#Task").html(scegli);
    $("select#Task").prop('disabled', 'disabled');
    $("select#Utenti").html(scegli);
    $("select#Utenti").prop('disabled', 'disabled');
    $("select#videoq").prop('disabled', 'disabled');

    /*
     * 
     * Settaggio iniziale
     */
    document.getElementById("btnCaricae").disabled = true;
    $.post("/utassistant/recVideoAudio/servizi.php", {Id_TaskS: Task, IdStudio: Studio}, function (data) {
        task = false;
        //alert("asd");
        $("select#Task").removeAttr("disabled");
        $("select#Task").html(data);
    });
    $.post("/utassistant/recVideoAudio/servizi.php", {Id_UtentiS: Utenti, IdStudio: Studio}, function (data) {
        utenti = false;
        $("select#Utenti").removeAttr("disabled");
        $("select#Utenti").html(data);
    });


    $('#btnCaricae').click(function () {
        co++;
        if (co == 1)
            window.open("/utassistant/app/esperto/valutazione.php ");
    });

    $("select#Task").change(function () {
        if ($("select#Task option:selected").attr('value') == "0") {
            task = false;
            document.getElementById("btnCaricae").disabled = true;
            $("select#videoq").html("<option>Scegli...</option>");
        } else {
            task = true;
        }
        caricaAudioVideo();
    });

    $("select#Utenti").change(function () {
        if ($("select#Utenti option:selected").attr('value') == "0")
        {
            utenti = false;
            document.getElementById("btnCaricae").disabled = true;
            $("select#videoq").html("<option>Scegli...</option>");
        } else {
            utenti = true;
        }
        caricaAudioVideo();
    });

    /**
     * riempie il filtro audio/video
     * @returns {undefined}
     */
    function caricaAudioVideo() {
        if (utenti == true && task == true) {
            verifica = true;
            var StudioScelto = sessionStorage.getItem('DStudio');
            var TaskScelto = $("select#Task option:selected").attr('value');
            var UtenteScelto = $("select#Utenti option:selected").attr('value');
            $.post("/utassistant/recVideoAudio/servizi.php", {
                Task: TaskScelto,
                Utente: UtenteScelto,
                Verifica: a[1]
            }, function (data) {
                $("select#videoq").html(data);
                $("select#videoq").removeAttr("disabled");
            });
            $.post("/utassistant/recVideoAudio/servizi.php", {
                Task: TaskScelto,
                Utente: UtenteScelto,
                Verifica: a[1]
            }, function (data) {
                $("select#audio").html(data);
                $("select#audio").removeAttr("disabled");
            });
        } else {
            $('select#Video').html(attendere);
            $('select#audio').html(attendere);
        }
    }
    if (a[1] == "video")
    {
        $("select#videoq").change(function () {
            if ($("select#videoq option:selected").attr('value') == "0") {
                document.getElementById("btnCaricae").disabled = true;
            } else {
                document.getElementById("btnCaricae").disabled = false;
                sessionStorage.setItem("datoVideo", $("select#videoq option:selected").attr('value'));
            }

        });
    } else {
        $("select#audio").change(function () {
            if ($("select#audio option:selected").attr('value') == "0") {
                document.getElementById("btnCaricae").disabled = true;
            } else {
                document.getElementById("btnCaricae").disabled = false;
                sessionStorage.setItem("datoAudio", $("select#audio option:selected").attr('value'));
            }

        });
    }

});

