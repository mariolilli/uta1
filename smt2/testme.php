<?php
// Start the session
session_start();
?>


<!DOCTYPE html>
<!-- This page is a bulletproof example to test smt2 recording capabilities. -->
<html>

<head>

  <title>My document title</title>
  
  <?php

    //Crea i cookie
    $cookie_name = "id_user";
    $cookie_value = $_SESSION["id_user"];

    setcookie($cookie_name, $cookie_value, time() + (86400 * 30),"/");

    $cookie_name = "id_task";
    $cookie_value = $_SESSION["idtask"];

    setcookie($cookie_name, $cookie_value, time() + (86400 * 30),"/");
	
	
    //Controllo se il tracciamento del mouse è abilitato
    if(isset($_SESSION["flag_comportamento"]))
	{
		$comportamento = $_SESSION["flag_comportamento"];

		if($comportamento == 1)
		{
			echo '<script type="text/javascript" src="http://localhost/utassistant/smt2/core/js/smt2e.min.js"></script>
			      <script type="text/javascript">
  				try 
  				{
    				smt2.record({ 
      				warn:false, 
      				warnText:"smt2e is going to track your cursor activity."});
  				} catch(err) {}
  			</script>';
		}
	}
  ?>

</head>

<body>

   TEST

</body>

</html>

