<?php
    echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>';

    // needed for async calls to this file

    if (!session_id()) {
        session_start();
    }
    $start = $page * $show - $show;
    /* Defining a relative path to smt2 root in this script is a bit tricky,
    * because this file can be called both from Ajax and regular HTML requests.
    */
    $base = realpath(dirname(__FILE__) . '/../../../../');
    require_once $base . '/config.php';

    // use ajax settings

    require_once dirname(__FILE__) . '/settings.php';

    include_once "C:\xampp\htdocs\utassistant\smt2\admin\sys\functions.db.php";

    $id_studio = $_SESSION['idstudio'];
    $where = "";

    if (isset($_SESSION['domain_id']) && $_SESSION['domain_id'] != 0) {
        $where.= " and r.domain_id = " . $_SESSION['domain_id'];
    }

    if (isset($_SESSION['cache_id']) && $_SESSION['cache_id'] != 0) {
        $where.= " and r.cache_id = " . $_SESSION['cache_id'];
    }

    if (isset($_SESSION['os_id']) && $_SESSION['os_id'] != 0) {
        $where.= " and r.os_id = " . $_SESSION['os_id'];
    }

    if (isset($_SESSION['browser_id']) && $_SESSION['browser_id'] != 0) {
        $where.= " and r.browser_id = " . $_SESSION['browser_id'];
    }

    // get ajax data

    if (!empty($_GET['page'])) {
        $page = $_GET['page'];
    }

    function getUserClicksAverage($id_user, $id_task, $where) {
        $queryClick = "SELECT r.clicks 
                       FROM smt2_records r 
                            JOIN smt2_ass_task_users_records tur ON r.id=tur.id_records 
                            JOIN task t ON t.id_task=tur.id_task 
                       WHERE tur.id_user LIKE '" . $id_user . "' AND t.id_task=" . $id_task . $where;
        $clicksRes = db_query_custom($queryClick);
        $numClick = 0;
        $records_total = 0;
        foreach ($clicksRes as $c => $rc) {
            $numClick += count_clicks($rc['clicks']);
            $records_total++;
        }
        return round($numClick / $records_total, 2);
    }

    function getUsersClickAverage($where) {
        $clicks_avgs = [];
        $queryUsersTask = "SELECT id_user, id_task FROM ass_user_task";
        $result_users = db_query_custom($queryUsersTask);
        $task_avgs = [];

        foreach ($result_users as $index => $value) {
            $user_id = $value['id_user'];
            $task_id = $value['id_task'];
            $user_avg = getUserClicksAverage($user_id, $task_id, $where);
            $clicks_avgs[$task_id][] = [
                'user' => $user_id,
                'avg' => $user_avg
            ];
        }

        foreach ($clicks_avgs as $task_id => $avg_data) {
            $n_tasks = count($clicks_avgs[$task_id]);
            $task_sum = 0;
            foreach ($avg_data as $avg_datum) {
                $task_sum += $avg_datum['avg'];
            }
            $task_avgs[$task_id] = round($task_sum / $n_tasks, 2);
        }

        return $task_avgs;
    }

    $tasks_avg_clicks = getUsersClickAverage($where);

    $query = "SELECT count(*) as notes, count(distinct cache_id) as n_pages,task.obiettivo, 
                     task.id_task, 
                     avg(r.sess_time) as mediaTempo,  
                     MIN(r.sess_date) as InizioPeriodo, 
                     MAX(r.sess_date) as FinePeriodo
              FROM smt2_ass_task_users_records tur 
                   join smt2_records r on tur.id_records=r.id 
                   join smt2_cache c on r.cache_id=c.id 
                   join users on tur.id_user=users.user_id 
                   join task on tur.id_task=task.id_task 
                   join studio on task.id_studio=studio.id_studio
              WHERE studio.id_studio=$id_studio AND r.client_id LIKE '" . $_SESSION['client_id'] . "' " . $where . " 
              GROUP BY task.id_task";
    $result = db_query_custom($query);
    $_SESSION['query'] = $query;
    $tablerow = "";
    $trId = 0;
    $btnId = 'B' . $trId;

    foreach($result as $i => $r) {
        $trId++;
        $btnId = 'B' . $trId;
        $displayid = $r['id_task'];
        $tablerow.= '<tr class="' . $cssClass . '">' . PHP_EOL;
        $tablerow.= "<td>" . $r['obiettivo'] . ' <span id="' . $btnId . '" class="glyphicon glyphicon-menu-down" title="Espandi"></span>' . PHP_EOL;
        $tablerow.= "<td>" . $r['n_pages'] . '</td>' . PHP_EOL;
        $tablerow.= "<td>" . $r['notes'] . '</td>' . PHP_EOL;
        $tablerow.= "<td>" . $r['InizioPeriodo'] . '</td>' . PHP_EOL;
        $tablerow.= "<td>" . $r['FinePeriodo'] . '</td>' . PHP_EOL;
        $tablerow.= "<td>" . round( ['mediaTempo'], 2) . '</td>' . PHP_EOL;
        $querypages = "select smt2_cache.url from smt2_ass_task_users_records tur join smt2_records r on tur.id_records=r.id join task on tur.id_task=task.id_task join smt2_cache on smt2_cache.id=r.cache_id where r.client_id LIKE '" . $_SESSION['client_id'] . "' and task.id_studio=" . $_SESSION['idstudio'] . " and tur.id_task=" . $r['id_task'] . $where;
        $pagesRes = db_query_custom($querypages);
        if (!$pagesRes) {

            //   die('<strong>Error</strong>: chaches #'.$id.' was not found on database.');

        }

        $queryClick = "SELECT r.clicks from smt2_records r join smt2_ass_task_users_records tur on r.id=tur.id_records join task t on
         t.id_task=tur.id_task where r.client_id LIKE '" . $_SESSION['client_id'] . "' and t.id_task=" . $r['id_task'] . $where;
        $clicksRes = db_query_custom($queryClick);
        $_SESSION['query2'] = $clicksRes;
        if (!$clicksRes) {

            //   die('<strong>Error</strong>: User log #'.$id.' was not found on database.');

        }


        $clickarray = [];
        foreach($clicksRes as $c => $rc) {
            $clickarray[] = count_clicks($rc['clicks']);
        }

        $numClick = $tasks_avg_clicks[$r['id_task']];
        $tablerow.= "<td>" . $numClick . '</td>' . PHP_EOL;

        $tablerow.= ' <td><a href="analyze.php?tid=' . $displayid . '" data-toggle="tooltip" data-placement="top" title="Analyze log"><span class="glyphicon glyphicon-list-alt"></span></a>';
        $tablerow.= ' <a href="download.php?tid=' . $displayid . '" data-toggle="tooltip" data-placement="top" title="Download log"><span class="glyphicon glyphicon-download-alt"></span></a></td></tr>' . PHP_EOL;
        $tablerow.= '<tr id="' . $trId . '" class="info" style="display:none">' . PHP_EOL;
        $tablerow.= " <td></td><td>";
        foreach($pagesRes as $t => $p) {
            $tablerow.= "<ul class='list-group', style='list-style-type:disc'>";
            $tablerow.= " <li class='list-group-item-info'>" . $p['url'] . "</li>";
        }

        $tablerow.= "</ul></td><td></td><td></td><td></td><td>";
        $timequery = "select r.sess_time as time from smt2_records r join smt2_ass_task_users_records tur on r.id = tur.id_records where tur.id_task = '" . $r['id_task'] . "' and r.client_id LIKE '" . $_SESSION['client_id'] . "' " . $where;
        $timeresult = db_query_custom($timequery);
        $_SESSION['query3'] = $timeresult;
        foreach($timeresult as $t => $p) {
            $tablerow.= "<ul class='list-group', style='list-style-type:disc'>";
            $tablerow.= " <li class='list-group-item-info'>" . $p['time'] . "</li>";
        }

        $tablerow.= "</ul></td><td>";
        foreach($clickarray as $c) {
            $tablerow.= "<ul class='list-group', style='list-style-type:disc'>";
            $tablerow.= " <li class='list-group-item-info'>" . $c . "</li>";
        }

        $tablerow.= "</ul></td>";
        echo ' <script>
        $(document).ready(function(){
        $("#' . $btnId . '").click(function () {
            if( $("#' . $btnId . '").hasClass("glyphicon glyphicon-menu-down")){
          $("#' . $trId . '").show(300);
           $("#' . $btnId . '").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up");
          }else{
            $("#' . $trId . '").hide(300);
           $("#' . $btnId . '").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down");
          }
        }); 
        });   
        </script>';
    }

    ECHO $tablerow;

    // check both normal and async (ajax) requests

    if ($start + $show < db_records()) {
        $displayMoreButton = true;
    }
    else {
        echo '<!--' . $noMoreText . '-->' . PHP_EOL;
    }

?>