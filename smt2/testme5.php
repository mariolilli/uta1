<?php
// Start the session
session_start();
?>


<!DOCTYPE html>
<!-- This page is a bulletproof example to test smt2 recording capabilities. -->
<HEAD>
   <TITLE>Esempio 1</TITLE>
         <?php /*

    //Crea i cookie
    $cookie_name = "id_user";
    $cookie_value = $_SESSION["id_user"];

    setcookie($cookie_name, $cookie_value, time() + (86400 * 30),"/");

    $cookie_name = "id_task";
    $cookie_value = $_SESSION["idtask"];

    setcookie($cookie_name, $cookie_value, time() + (86400 * 30),"/");
	
	
    //Controllo se il tracciamento del mouse è abilitato
    if(isset($_SESSION["flag_comportamento"]))
	{
		$comportamento = $_SESSION["flag_comportamento"];

		if($comportamento == 1)
		{
			echo '<script type="text/javascript" src="http://localhost/userpie/smt2/core/js/smt2e.min.js"></script>
			      <script type="text/javascript">
  				try 
  				{
    				smt2.record({ 
      				warn:true, 
      				warnText:"smt2e is going to track your cursor activity."});
  				} catch(err) {}
  			</script>';
		}
	} */
  ?>
</HEAD>

<BODY BACKGROUND="titto_green_paper.jpg">

<CENTER><H1>Questo &egrave; scritto in stile Heading 1</H1></CENTER>

<FONT SIZE=+2>

Scritta non indentata

<UL>      <!-- questo marcatore UL segna l'inizio
               di una Unordered List, che non conterra'
               nessun LI (List Item), l'effetto sara' 
               quello di una semplice indentatura.   -->

  Scritta indentata seguita da un break<BR>
  il break mi ha portato a riga nuova<P>
    il paragrafo stacca di piu' (e permette di cambiare stile)
</UL>     <!-- fine dell'indentatura -->

<UL>      <!-- questa è una lista vera, invece -->
  <LI>elemento uno</LI>
  <LI>elemento due</LI>
</UL>

<CENTER>scritta centrata</CENTER>

</FONT>
<HR WIDTH="100%">       <!-- una Hard Line che prendera' 
                             il 100% della pagina -->

</BODY>
</HTML>

